// Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum)

const secondNUm = Math.pow(8,2);
console.log(secondNUm) //before ES6

//Template Literals
/*
		Allows to write strings without using the concatenation operator

*/
let name = "Nehemiah";

//Pre-Template Literal String
//Using single quote
let message = 'Hello ' + name + '. Welcome to programming!';
console.log("Message without the template letrals: " + message)
//Strings Using Template Literal
//Uses the backticks (``)
message = `Hello ${name}. Welcome to programming!`
console.log(message)

let anotherMessage = `
${name} attended a math competition.
He wont it by solving the problem 8 **2 with the solution of ${firstNum}.
`
console.log(anotherMessage)

anotherMessage = "\n" + name + ' attended a math competition. \n He won it by solving the prolme 8**2 with the solution of ' + firstNum + ". \n"
console.log(anotherMessage)

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings is: ${principal * interestRate}`)

//Array Destructuring
/*
	Allows us to unpack elements in arrays to distinct variables. Allows us to name array elements with variables instead of index numbers.

	Syntax:
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Joe", "Dela", "Cruz"]

//Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`)

//Array Destructuring
const [firstName, middleName, lastName] = fullName
console.log(firstName)
console.log(middleName)
console.log(lastName)
console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

//Object Destructuring
/*
	Allows to unpack properties of objects into distinct variables. Shortens the syntax for accessing properties from objects.

	Syntax:
		let/const { propertyName, propertyName, propertyName} = object;
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}
//Pre-Object Destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

const { maidenName, givenName, familyName} = person
console.log(givenName);
console.log(maidenName);
console.log(familyName);

function getFullName({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person)

//Arror Functions

/*
	Alternative syntax to traditional functions

	const variableName = () => {
		console.log
	}


*/

//arrow function
const hello = () => {
		console.log("Hello World")
	}

hello()



//traditional way
function greeting () {
	console.log("Hello World")
}

greeting()

//Pre-Arrow Function
/*
	Syntax:
		function functionName (parameterA, parameterB) {
			console.log
		}

*/

function printFullName (firstName, middleInitial, lastName) {
			console.log(firstName+ " " + middleInitial + " " + lastName)
		}

printFullName("Amiel", "D", "Oliva")

//Arrow Function
/*
	Syntax: 
		let/const variableName = (parameterA, parameterB) => {
			console.log()
		}


*/

const printFullName1= (firstName, middleInitial, lastName) => {
			console.log(`${firstName} ${middleInitial} ${lastName}`);
		}


printFullName("Charles Patrick", "A", "Lilagan")


const students = ["Rupert", "Carlos", "Jerome"]


//Pre-arrow Function

students.forEach(function(student) {
	console.log(`${student} is a student.`);
})

//Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`);
})


//Pre-Arrow Function

function add(x,y) {
	return x + y
}

let total = add(12,15)
console.log(total)


//Arrow Function 
const addition = (x, y) => x + y;


//const - no return statement otherwise enclose 'return' with a curly braces
/*const addition = (x, y) => {
	return x + y

}*/

let resultOfAad = addition(12,15)
console.log(resultOfAad)


//Default Function Argument Value

const greet = (name = "User") => {
	return `Good Morning, ${name}!`
}

console.log(greet());
console.log(greet(`Grace`));


//Class-Based Object Blueprints

/*
	Allows creation/instatiation of objects using classes as blueprints

	Syntax:
		class className {
			constructor (objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}


*/


class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year
	}
}

const myCar = new Car()
console.log(myCar)

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar)


const myNewCar = new Car (`Toyota`, `Vios`, 2021);
console.log(myNewCar)



